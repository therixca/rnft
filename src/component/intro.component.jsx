import React from "react";

function IntroComponent() {
  return (
    <div className="text-gray-50">
      <div className="text-xl font-bold">RIXCA nft's Galery</div>
	    <p>
		    A public galery for RIXCA nft arts, You can see collection or single nft befor you decide to collect them.
	    </p>
    </div>
  );
}

export default IntroComponent;
